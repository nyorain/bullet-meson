# Pre-built bullet3 library as meson subproject

Built in November 2019 on Arch with
```
cmake -DINSTALL_LIBS=true -DINCLUDE_INSTALL_DIR=include/bullet
```
